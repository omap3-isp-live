/*
 * OMAP3 ISP snapshot test application
 *
 * Copyright (C) 2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __JPEG_H__
#define __JPEG_H__

#include <stdio.h>

#include <linux/v4l2-mediabus.h>

#include "isp/v4l2-pool.h"

struct jpeg;

struct jpeg *jpeg_init(const struct v4l2_mbus_framefmt *format);
void jpeg_cleanup(struct jpeg *jpeg);

int jpeg_save(struct jpeg *jpeg, FILE *file, struct v4l2_video_buffer *buffer);

#endif
