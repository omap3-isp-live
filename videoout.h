/*
 * V4L2 video output
 *
 * Copyright (C) 2010-2011 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __VIDEOOUT_H__
#define __VIDEOOUT_H__

#include <linux/videodev2.h>

#include "isp/v4l2-pool.h"

struct videoout;

struct video_out_operations {
	void (*watch_fd)(int fd);
	void (*unwatch_fd)(int fd);
};

struct videoout *vo_init(const char *devname,
			 const struct video_out_operations *ops,
			 unsigned int buffers,
			 struct v4l2_pix_format *format,
			 unsigned int rotation);
void vo_cleanup(struct videoout *vo);

int vo_enable_colorkey(struct videoout *vo, unsigned int val);
int vo_disable_colorkey(struct videoout *vo);

struct v4l2_buffers_pool *vo_get_pool(struct videoout *vo);
int vo_dequeue_buffer(struct videoout *vo, struct v4l2_video_buffer *buffer);
int vo_queue_buffer(struct videoout *vo, struct v4l2_video_buffer *buffer);

#endif /* __VIDEOOUT_H__ */
