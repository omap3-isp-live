/*
 * OMAP3 ISP library - OMAP3 ISP private header
 *
 * Copyright (C) 2010-2011 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __OMAP3ISP_PRIV_H
#define __OMAP3ISP_PRIV_H

#include <linux/omap3isp.h>
#include <linux/v4l2-mediabus.h>

#include "omap3isp.h"
#include "stats-priv.h"
#include "tools.h"

struct omap3_isp_device;

/*
 * struct omap3_isp_pad - OMAP3 entity pad
 * @link: Link connected to the pad
 * @crop: Crop rectangle
 * @format: Format on the pad
 */
struct omap3_isp_pad {
	struct media_entity_link *link;
	struct v4l2_rect crop;
	struct v4l2_mbus_framefmt format;
};

enum omap3_isp_entity_type {
	OMAP3_ISP_ENTITY_ENTITY,
	OMAP3_ISP_ENTITY_POOL,
	OMAP3_ISP_ENTITY_VIDEO,
};

/*
 * struct omap3_isp_entity - OMAP3 entity in a pipeline
 * @list: Entities list
 * @type: Entity type
 * @entity: Media entity information
 * @sink: Sink pad
 * @source: Sink pad
 * @last: Indicates whether this is the last entity in the pipeline
 */
struct omap3_isp_entity {
	struct list_entry list;
	enum omap3_isp_entity_type type;
	struct media_entity *entity;
	struct omap3_isp_pad sink;
	struct omap3_isp_pad source;
	bool last;
};

/*
 * struct omap3_isp_video - OMAP3 video device
 * @entity: OMAP3 entity
 * @isp: OMAP3 ISP device
 * @video: V4L2 capture device
 * @format: Current video format
 * @pool: Buffers pool
 * @queued: Number of buffers queued to the driver
 * @running: Whether video capture is running on the device
 */
struct omap3_isp_video {
	struct omap3_isp_entity entity;
	struct omap3_isp_device *isp;
	struct v4l2_device *video;
	struct v4l2_mbus_framefmt format;
	struct v4l2_buffers_pool *pool;
	unsigned int queued;
	bool running;
};

#define to_omap3_isp_video(e)	container_of(e, struct omap3_isp_video, entity)

/*
 * struct omap3_isp_pool - OMAP3 buffers pool
 * @entity: OMAP3 entity
 * @list: Pools list
 * @isp: OMAP3 ISP device
 * @input: Capture video device
 * @output: Output video device
 * @pool: Buffers pool
 */
struct omap3_isp_pool {
	struct omap3_isp_entity entity;
	struct list_entry list;
	struct omap3_isp_device *isp;
	struct omap3_isp_video *input;
	struct omap3_isp_video *output;
	struct v4l2_buffers_pool *pool;
};

#define to_omap3_isp_pool(e)	container_of(e, struct omap3_isp_pool, entity)

/*
 * struct omap3_isp_resizer - OMAP3 ISP resizer parameters in a pipeline
 * @entity: The resizer entity
 * @zoom_min: Crop rectangle at minimum zoom
 * @zoom_max: Crop rectangle at maximum zoom (up to x4.0)
 * @pan_x: Zoom center along X axis
 * @pan_y: Zoom center along Y axis
 * @zoom: Zoom level (0.0 - 1.0)
 */
struct omap3_isp_resizer {
	struct omap3_isp_entity *entity;
	struct v4l2_rect zoom_min;
	struct v4l2_rect zoom_max;
};

enum omap3_isp_pipeline_state {
	OMAP3_ISP_PIPELINE_STOPPED,
	OMAP3_ISP_PIPELINE_RUNNING,
	OMAP3_ISP_PIPELINE_SUSPENDED,
};

/*
 * struct omap3_isp_pipeline - OMAP3 pipeline
 * @entitites: Entities in the pipeline
 * @scaler: Whether scaling should be performed on the ISP or the sensor
 * @pools: Pools in the pipeline
 * @input: Video device at the input of the pipeline
 * @output: Video device at the output of the pipeline
 * @state: Pipeline state
 */
struct omap3_isp_pipeline {
	struct list_entry entities;
	enum omap3_isp_scaler scaler;
	struct list_entry pools;
	struct omap3_isp_video *input;
	struct omap3_isp_video *output;
	enum omap3_isp_pipeline_state state;
	struct omap3_isp_resizer resizer;
};

enum omap3_isp_state {
	OMAP3_ISP_IDLE,
	OMAP3_ISP_VIEWFINDER,
	OMAP3_ISP_SNAPSHOT,
	OMAP3_ISP_SCALER,
};

struct omap3_isp_device {
	struct media_device *mdev;

	struct {
		struct media_entity *entity;
		struct v4l2_mbus_framefmt format;
		bool has_color_gains;
		bool has_global_gain;
	} sensor;
	struct {
		struct media_entity *entity;
	} ccdc;
	struct {
		struct media_entity *entity;
		struct omap3isp_prev_wbal wbal;
	} preview;
	struct {
		struct media_entity *entity;
	} resizer;
	struct omap3_isp_aewb aewb;

	struct omap3_isp_pipeline viewfinder;
	struct omap3_isp_pipeline snapshot;
	struct omap3_isp_pipeline scaler;

	enum omap3_isp_state state;

	const struct omap3_isp_operations *ops;
};

#endif
