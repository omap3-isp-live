/*
 * OMAP3 ISP library - OMAP3 ISP statistics
 *
 * Copyright (C) 2010-2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __OMAP3ISP_STATS_H
#define __OMAP3ISP_STATS_H

#include <stdint.h>

struct omap3_isp_device;

/*
 * struct omap3_isp_aewb_stats - OMAP3 ISP AEWB statistics
 * @npix: Total number of accumulated pixels
 * @unsat: Number of accumulated unsaturated pixels
 * @accum: Accumulators
 *
 * The first 4 accumulators store the accumulated pixel values for the 4 color
 * components, organized as follows.
 *
 * +---+---+
 * | 0 | 1 |
 * +---+---+
 * | 2 | 3 |
 * +---+---+
 *
 * The next 4 accumulators are similar, but store the accumulated saturated
 * (clipped) pixel values.
 */
struct omap3_isp_aewb_stats {
	uint32_t npix;
	uint32_t unsat;
	uint32_t accum[8];
};

/*
 * omap3_isp_stats_enable - Enable or disable the statistics engine
 * @isp: The ISP device
 * @enable: Whether to enable to disable the statistics engine
 *
 * The statistics engine must be enabled prior to starting the video stream.
 * When enabled, it statistics will be computed for every frame and delivered
 * through the ISP aewb_ready() callback.
 */
void omap3_isp_stats_enable(struct omap3_isp_device *isp, bool enable);

/*
 * omap3_isp_stats_get_format - Get frame format at the statistics engine input
 * @isp: The ISP device
 * @format: Frame format
 *
 * Fill the format argument with the frame format at the statistics engine
 * input.
 */
int omap3_isp_stats_get_format(struct omap3_isp_device *isp,
			       struct v4l2_mbus_framefmt *format);
/*
 * omap3_isp_aewb_configure - Configure the AEWB statistics engine
 * @isp: The ISP device
 * @rect: The region of interest, relative to the sensor output frame size
 * @saturation: Pixel saturation threshold value
 */
int omap3_isp_aewb_configure(struct omap3_isp_device *isp, struct v4l2_rect *rect,
			     unsigned int saturation);

#endif
