/*
 * OMAP3 ISP library - V4L2 sub-devices
 *
 * Copyright (C) 2010-2011 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __SUBDEV_H__
#define __SUBDEV_H__

#include <linux/v4l2-subdev.h>

struct media_entity;

/*
 * v4l2_subdev_open - Open a sub-device
 * @entity: Sub-device media entity
 *
 * Open the V4L2 subdev device node associated with @entity. The file descriptor
 * is stored in the media_entity structure.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_open(struct media_entity *entity);

/*
 * v4l2_subdev_close - Close a sub-device
 * @entity: Sub-device media entity
 *
 * Close the V4L2 subdev device node associated with the @entity and opened by
 * a previous call to v4l2_subdev_open() (either explicit or implicit).
 */
void v4l2_subdev_close(struct media_entity *entity);

/*
 * v4l2_subdev_get_format - Retrieve the format on a pad
 * @entity: Subdev-device media entity
 * @format: Format to be filled
 * @pad: Pad number
 * @which: Identifier of the format to get
 *
 * Retrieve the current format on the @entity @pad and store it in the @format
 * structure.
 *
 * @which is set to V4L2_SUBDEV_FORMAT_TRY to retrieve the try format stored in
 * the file handle, of V4L2_SUBDEV_FORMAT_ACTIVE to retrieve the current active
 * format.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_get_format(struct media_entity *entity,
	struct v4l2_mbus_framefmt *format, unsigned int pad,
	enum v4l2_subdev_format_whence which);

/*
 * v4l2_subdev_set_format - Set the format on a pad
 * @entity: Subdev-device media entity
 * @format: Format
 * @pad: Pad number
 * @which: Identifier of the format to set
 *
 * Set the format on the @entity @pad to @format. The driver is allowed to
 * modify the requested format, in which case @format is updated with the
 * modifications.
 *
 * @which is set to V4L2_SUBDEV_FORMAT_TRY to set the try format stored in the
 * file handle, of V4L2_SUBDEV_FORMAT_ACTIVE to configure the device with an
 * active format.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_set_format(struct media_entity *entity,
	struct v4l2_mbus_framefmt *format, unsigned int pad,
	enum v4l2_subdev_format_whence which);

/*
 * v4l2_subdev_get_crop - Retrieve the crop rectangle on a pad
 * @entity: Subdev-device media entity
 * @rect: Crop rectangle to be filled
 * @pad: Pad number
 * @which: Identifier of the format to get
 *
 * Retrieve the current crop rectangleon the @entity @pad and store it in the
 * @rect structure.
 *
 * @which is set to V4L2_SUBDEV_FORMAT_TRY to retrieve the try crop rectangle
 * stored in the file handle, of V4L2_SUBDEV_FORMAT_ACTIVE to retrieve the
 * current active crop rectangle.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_get_crop(struct media_entity *entity, struct v4l2_rect *rect,
	unsigned int pad, enum v4l2_subdev_format_whence which);

/*
 * v4l2_subdev_set_crop - Set the crop rectangle on a pad
 * @entity: Subdev-device media entity
 * @crop: Crop rectangle
 * @pad: Pad number
 * @which: Identifier of the format to set
 *
 * Set the crop rectangle on the @entity @pad to @rect. The driver is allowed to
 * modify the requested rectangle, in which case @rect is updated with the
 * modifications.
 *
 * @which is set to V4L2_SUBDEV_FORMAT_TRY to set the try crop rectangle stored
 * in the file handle, of V4L2_SUBDEV_FORMAT_ACTIVE to configure the device with
 * an active crop rectangle.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_set_crop(struct media_entity *entity, struct v4l2_rect *rect,
	unsigned int pad, enum v4l2_subdev_format_whence which);

/*
 * v4l2_subdev_set_frame_interval - Set the frame interval on a sub-device
 * @entity: Subdev-device media entity
 * @interval: Frame interval
 *
 * Set the frame interval on subdev @entity to @interval. The driver is allowed
 * to modify the requested frame interval, in which case @interval is updated
 * with the modifications.
 *
 * Frame interval setting is usually supported only on devices at the beginning
 * of video pipelines, such as sensors.
 *
 * Return 0 on success, or a negative error code on failure.
 */
int v4l2_subdev_set_frame_interval(struct media_entity *entity,
	struct v4l2_fract *interval);

/*
 * v4l2_subdev_get_control - Read the value of a control
 * @entity: Subdev-device media entity
 * @id: Control ID
 * @value: Control value to be filled
 *
 * Retrieve the current value of control @id and store it in @value.
 *
 * Return 0 on success or a negative error code on failure.
 */
int v4l2_subdev_get_control(struct media_entity *entity, unsigned int id,
			    int32_t *value);

/*
 * v4l2_subdev_set_control - Write the value of a control
 * @entity: Subdev-device media entity
 * @id: Control ID
 * @value: Control value
 *
 * Set control @id to @value. The device is allowed to modify the requested
 * value, in which case @value is updated to the modified value.
 *
 * Return 0 on success or a negative error code on failure.
 */
int v4l2_subdev_set_control(struct media_entity *entity, unsigned int id,
			    int32_t *value);

/*
 * v4l2_subdev_get_controls - Read the value of multiple controls
 * @entity: Subdev-device media entity
 * @count: Number of controls
 * @ctrls: Controls to be read
 *
 * Retrieve the current value of controls identified by @ctrls.
 *
 * Return 0 on success or a negative error code on failure.
 */
int v4l2_subdev_get_controls(struct media_entity *entity, unsigned int count,
			     struct v4l2_ext_control *ctrls);

/*
 * v4l2_subdev_set_controls - Write the value of multiple controls
 * @entity: Subdev-device media entity
 * @count: Number of controls
 * @ctrls: Controls to be written
 *
 * Set controls identified by @ctrls. The device is allowed to modify the
 * requested values, in which case @ctrls is updated to the modified value.
 *
 * Return 0 on success or a negative error code on failure.
 */
int v4l2_subdev_set_controls(struct media_entity *entity, unsigned int count,
			     struct v4l2_ext_control *ctrls);

#endif
