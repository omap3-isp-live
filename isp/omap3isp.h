/*
 * OMAP3 ISP library - OMAP3 ISP
 *
 * Copyright (C) 2010-2011 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __OMAP3ISP_H
#define __OMAP3ISP_H

#include <linux/v4l2-mediabus.h>
#include <sys/select.h>
#include <stdbool.h>

#include "media.h"
#include "stats.h"
#include "v4l2.h"

struct omap3_isp_device;

enum omap3_isp_scaler {
	OMAP3_ISP_SCALER_ISP = 0,
	OMAP3_ISP_SCALER_SENSOR = 1,
};

enum omap3_isp_event_type {
	OMAP3_ISP_EVENT_READ = 1,
	OMAP3_ISP_EVENT_WRITE = 2,
	OMAP3_ISP_EVENT_EXCEPTION = 4,
};

struct omap3_isp_operations {
	void (*prepare_streamon)(struct omap3_isp_device *isp);
	void (*viewfinder_ready)(struct omap3_isp_device *isp,
				 struct v4l2_video_buffer *buffer);
	void (*snapshot_ready)(struct omap3_isp_device *isp,
			       struct v4l2_video_buffer *buffer);
	void (*scaler_ready)(struct omap3_isp_device *isp,
			     struct v4l2_video_buffer *buffer);
	void (*aewb_ready)(struct omap3_isp_device *isp,
			   const struct omap3_isp_aewb_stats *stats);
	void (*watch_fd)(int fd, enum omap3_isp_event_type type,
			 void(*callback)(void *priv), void *priv);
	void (*unwatch_fd)(int fd);
};

struct omap3_isp_device *omap3_isp_open(const char *devname,
					const struct omap3_isp_operations *ops);
void omap3_isp_close(struct omap3_isp_device *isp);

/* Viewfinder */
int omap3_isp_viewfinder_setup(struct omap3_isp_device *isp,
			       struct v4l2_rect *crop,
			       struct v4l2_mbus_framefmt *ofmt);
int omap3_isp_viewfinder_alloc_pool(struct omap3_isp_device *isp,
				    unsigned int nbufs);
void omap3_isp_viewfinder_free_pool(struct omap3_isp_device *isp);
struct v4l2_buffers_pool *
omap3_isp_viewfinder_get_pool(struct omap3_isp_device *isp);
int omap3_isp_viewfinder_set_pool(struct omap3_isp_device *isp,
				  struct v4l2_buffers_pool *pool);
int omap3_isp_viewfinder_set_scaler(struct omap3_isp_device *isp,
				    enum omap3_isp_scaler scaler);
int omap3_isp_viewfinder_start(struct omap3_isp_device *isp, unsigned int bufs);
int omap3_isp_viewfinder_stop(struct omap3_isp_device *isp);
int omap3_isp_viewfinder_put_buffer(struct omap3_isp_device *isp,
				    struct v4l2_video_buffer *buffer);
int omap3_isp_viewfinder_pan_zoom(struct omap3_isp_device *isp,
				  float x, float y, float zoom);

/* Snapshot */
int omap3_isp_snapshot_setup(struct omap3_isp_device *isp,
			     struct v4l2_rect *crop,
			     struct v4l2_mbus_framefmt *ofmt);
int omap3_isp_snapshot_capture(struct omap3_isp_device *isp);
int omap3_isp_snapshot_done(struct omap3_isp_device *isp);
int omap3_isp_snapshot_put_buffer(struct omap3_isp_device *isp,
				  struct v4l2_video_buffer *buffer);

/* Scaler */
int omap3_isp_scaler_setup(struct omap3_isp_device *isp,
			   struct v4l2_mbus_framefmt *ifmt,
			   struct v4l2_mbus_framefmt *ofmt);
int omap3_isp_scaler_set_pool(struct omap3_isp_device *isp,
			      struct v4l2_buffers_pool *pool);
int omap3_isp_scaler_start(struct omap3_isp_device *isp,
			   struct v4l2_video_buffer *ibuf,
			   unsigned int obufs);
int omap3_isp_scaler_stop(struct omap3_isp_device *isp);
int omap3_isp_scaler_put_buffer(struct omap3_isp_device *isp,
				struct v4l2_video_buffer *buffer);
int omap3_isp_scaler_pan_zoom(struct omap3_isp_device *isp,
			      float x, float y, float zoom);

/* Processing parameters */
#define OMAP3_ISP_SENSOR_GAIN_KEEP	-1

int omap3_isp_sensor_get_exposure(struct omap3_isp_device *isp,
				  unsigned int *exposure);
int omap3_isp_sensor_set_exposure(struct omap3_isp_device *isp,
				  unsigned int exposure);
int omap3_isp_sensor_set_gain(struct omap3_isp_device *isp, unsigned int gain);
int omap3_isp_sensor_set_gains(struct omap3_isp_device *isp,
			       int red, int green, int blue);

int omap3_isp_ccdc_set_black_level(struct omap3_isp_device *isp, unsigned int value);

int omap3_isp_preview_set_contrast(struct omap3_isp_device *isp, unsigned int value);
int omap3_isp_preview_set_saturation(struct omap3_isp_device *isp, float value);
int omap3_isp_preview_set_gain(struct omap3_isp_device *isp, float gain);
int omap3_isp_preview_set_white_balance(struct omap3_isp_device *isp, float gains[4]);

#endif
