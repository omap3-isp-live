/*
 * OMAP3 ISP library - OMAP3 ISP statistics private header
 *
 * Copyright (C) 2010-2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef __OMAP3ISP_STATS_PRIV_H
#define __OMAP3ISP_STATS_PRIV_H

#include <stdint.h>

#include <linux/omap3isp.h>
#include <linux/v4l2-mediabus.h>
#include <linux/videodev2.h>

#include "stats.h"

/*
 * struct omap3_isp_aewb - OMAP3 ISP AEWB statistics engine
 * @entity: Automatic Exposure & White Balance entity
 * @size: buffer size
 * @buffer: buffer memory
 * @rect: AEWB region of interest
 * @win_x: horizontal coordinate of the left-most window
 * @win_y: vertical coordinate of the top-most window
 * @win_n_x: horizontal number of windows
 * @win_n_y: vertical number of windows
 * @win_w: window width
 * @win_h: window height
 * @win_inc_x: window horizontal increment
 * @win_inc_y: window vertical increment
 * @enabled: statistics engine enabled
 */
struct omap3_isp_aewb {
	struct media_entity *entity;
	unsigned int size;
	void *buffer;

	unsigned int win_x;
	unsigned int win_y;
	unsigned int win_n_x;
	unsigned int win_n_y;
	unsigned int win_w;
	unsigned int win_h;
	unsigned int win_inc_x;
	unsigned int win_inc_y;

	unsigned int saturation;

	bool enabled;
};

int omap3_isp_stats_start(struct omap3_isp_device *isp);
void omap3_isp_stats_stop(struct omap3_isp_device *isp);
int omap3_isp_stats_init(struct omap3_isp_device *isp);
void omap3_isp_stats_cleanup(struct omap3_isp_device *isp);

#endif
