/*
 * OMAP3 ISP library - OMAP3 ISP controls
 *
 * Copyright (C) 2010-2011 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#ifndef __OMAP3ISP_CONTROLS_H
#define __OMAP3ISP_CONTROLS_H

struct omap3_isp_device;

int omap3_isp_preview_setup(struct omap3_isp_device *isp);
int omap3_isp_sensor_init(struct omap3_isp_device *isp);

#endif /* __OMAP3ISP_CONTROLS_H */
