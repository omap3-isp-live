/*
 * OMAP3 image quality tuning
 *
 * Copyright (C) 2010-2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __IQ_H__
#define __IQ_H__

struct iq_tuning;
struct omap3_isp_device;
struct omap3_isp_aewb_stats;

/**
 * struct iq_params - Image quality tuning parameters
 * @window: Statistics computation window (fraction of the image size)
 * @ae_delay: Number of frames to skip at stream start before enabling AE
 * @ae_interval: Number of frames between AE algorithm runs
 * @mean_level: Target mean luminance level (fraction of the maximum)
 * @exposure: Initial exposure value (sensor-specific units)
 * @exposure_min: Minimum exposure value
 * @exposure_max: Maximum exposure value
 * @gain: Initial gain value (sensor-specific units)
 * @gain_min: Minimum gain value
 * @gain_max: Maximum gain value
 * @black_level: Black level offset
 * @saturation: Color saturation
 */
struct iq_params {
	struct {
		float left;
		float top;
		float width;
		float height;
	} window;

	unsigned int ae_delay;
	unsigned int ae_interval;

	float mean_level;

	unsigned int exposure;
	unsigned int exposure_min;
	unsigned int exposure_max;

	unsigned int gain;
	unsigned int gain_min;
	unsigned int gain_max;

	unsigned int black_level;
	float saturation;
};

void iq_aewb_process(struct iq_tuning *iq,
		     const struct omap3_isp_aewb_stats *stats);

void iq_params_init(struct iq_params *params);
int iq_params_parse(struct iq_params *params, const char *arg);

struct iq_tuning *iq_init(struct omap3_isp_device *isp,
			  const struct iq_params *params);
void iq_cleanup(struct iq_tuning *iq);

#endif
