/*
 * OMAP3 generic events handling
 *
 * Copyright (C) 2010-2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __EVENTS_H__
#define __EVENTS_H__

#include <stdbool.h>
#include <sys/select.h>

#include "isp/list.h"

struct events {
	struct list_entry events;
	bool done;

	int maxfd;
	fd_set rfds;
	fd_set wfds;
	fd_set efds;
};

void events_watch_fd(struct events *events, int fd,
		     enum omap3_isp_event_type type,
		     void(*callback)(void *), void *priv);
void events_unwatch_fd(struct events *events, int fd);

bool events_loop(struct events *events);
void events_stop(struct events *events);

void events_init(struct events *events);

#endif
