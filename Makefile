CROSS_COMPILE ?=
KDIR ?=
STATIC ?=

CC	:= $(CROSS_COMPILE)gcc
CFLAGS	:= -O2 -W -Wall -I$(KDIR)/usr/include
LDFLAGS	:= -Lisp
LIBS	:= -lomap3isp -lpthread -lrt

ifneq (x$(STATIC), x)
LIBS     += -static
LIB_ISP  := isp/libomap3isp.a
else
LIB_ISP  := isp/libomap3isp.so
endif

OBJ_LIVE := live.o events.o iq.o videoout.o
OBJ_SNAP := snapshot.o events.o iq.o

ifneq (x$(LIBJPEG), x)
CFLAGS += -I$(LIBJPEG)/include -DUSE_LIBJPEG=1
SNAP_LDFLAGS = -L$(LIBJPEG)/lib
SNAP_LIBS = -ljpeg
OBJ_SNAP += jpeg.o
endif

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

all: __isp live snapshot

live: $(OBJ_LIVE) $(LIB_ISP)
	$(CC) $(LDFLAGS) -o $@ $(OBJ_LIVE) $(LIBS)

snapshot: $(OBJ_SNAP) $(LIB_ISP)
	$(CC) $(LDFLAGS) $(SNAP_LDFLAGS) -o $@ $(OBJ_SNAP) $(LIBS) $(SNAP_LIBS)

__isp:
	$(MAKE) -C isp CROSS_COMPILE=$(CROSS_COMPILE) KDIR=$(KDIR)

clean:
	$(MAKE) -C isp clean
	-$(RM) *.o
	-$(RM) live
	-$(RM) snapshot

